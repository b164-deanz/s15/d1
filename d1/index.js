console.log("Hello world")
console.log("Hello world")

//Assignment Operator
let assignmentNumber = 8;

//Arithmetic Operators
//+ - / %

//Addition Assignment Operator(+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber);

//addition shorthand
assignmentNumber += 2;
console.log(assignmentNumber)

//subtract/multiply/divide assignment operator
assignmentNumber -= 2;
console.log(assignmentNumber)

assignmentNumber *= 2;
console.log(assignmentNumber)
assignmentNumber /= 2;
console.log(assignmentNumber)

//Multiple Operators and Parenthesis
/*
-when multiple operators are applied in a single statements, it follows the PEMDAS(Parenthesis, exponent, multiplication, division, addition, subtractions)

-operations were done in the ff order:
1. 3 * 4 =12
2. 12 / 5 = 2.4
3. 1 + 2 = 3
4. 3 - 2 = 1
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas)

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas)

//Increment and Decrement Operator
//Operators that add or subtract values by 1 and reassignes the value of the variable where the increment/decrement was applied to

let z =1;

//increment
//pre-fix incrementation.
//The value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment".
++z;
console.log(z) //2 - the value of z was added with 1 and is immediately returned.

//postfix incrementation
//the value of "z" is returned and stored in the variable "increment" and the value of "z" is increased by 1.
z++;
console.log(z); //3 - the value of z was added with 1
console.log(z++) //3 - the previous value of the variable is returned
console.log(z) //4 - a new value is no returned

//pre-fix vs postfix incrementation
console.log(z++)//4
console.log(z) //5

console.log(++z) //6

//prefix and postfix decrementation
console.log(--z); //5 - with pre-fix decrementation the result of subtraction by 1 is returned immediately

console.log(z--) //5 with postfix decrementation the result of subtraction by 1 is immediatly returned, instead the previous value is returned first
console.log(z) //a


//Type Coercion
//is the automatic or implicit conversion of values from one data type to another
let num1 = '10'
let num2 = 12;
let coercion = num1 + num2;
console.log(coercion)
console.log(typeof coercion)

//adding or concatinating a string or a number will result to a string

let numc = 16;
let numd = 14;

let numCoercion = numc + numd;
console.log(numCoercion)
console.log(typeof numCoercion)

//the boolean "true" is also associated with the value of 1
let numE = true + 1;
console.log(numE)
console.log(typeof numE)


//the boolean "false" is also associated with the value of 0
let numf = false + 1;
console.log(numf)

//Comparison operators
let juan = 'juan'

// (==) equality operator
//checks whether the operands are equal or have the same content
//attemps to convert and compare operands of different data types.
//returns a boolean value

console.log(1 ==1);//true
console.log(1==2)//false
console.log(1=='1') //true
console.log(0 == false)//true
console.log('juan' == 'JUAN') //false
console.log('juan' == juan) //true

//(!=) Inequality Operator
//checks whether the operands are not equal or have different content

console.log(1 != 1)//false
console.log(1 != 2)//true
console.log(1 != '1')//false
console.log(0!=false)//false
console.log('juan' != 'JUAN')//true
console.log('juan' != juan)//false

console.log("")
//Strictly Equality Operator
console.log(1 ===1);//true
console.log(1===2)//false
console.log(1==='1') //false = not the same data type
console.log(0 === false)//false = different data type
console.log('juan' === 'JUAN') //false , case sensitive
console.log('juan' === juan) //true - same string

console.log("")
//String Inequality Operator
console.log(1 !== 1)//false
console.log(1 !== 2)//true
console.log(1 !== '1')//true
console.log(0!==false)//true
console.log('juan' !== 'JUAN')//true
console.log('juan' !== juan)//false


//Relational Comparison Operators
//check the relationship between the operands

let x =500;
let y = 700;
let w = 8000;
let numString = "5500"

console.log("greather than")
//Greater than (>)
console.log(x > y) //false
console.log(w > y)// true

console.log("Less Than")
//less than (<)
console.log(w<x); //false
console.log(y < y)//false
console.log(x < 1000) //true
console.log(numString < 1000) //false - forced coercion
console.log(numString < 6000) //true - forced coercion to change string to number

console.log(numString < "Jose") //true

//greater than or equal
console.log(w >= 8000) //true

//less than or equal to
console.log(x <= y) //true
console.log(y <= y) //true

//Logical Operators
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("Logical Operators")
//Logical AND Operator (&& - double ampersand)
//Returns true is all operands are true
let authorization1 = isAdmin && isRegistered;
console.log(authorization1) //false

let authorization2 = isLegalAge && isRegistered
console.log(authorization2) //true

let requiredLevel = 95;
let requiredAge = 18;
let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3)

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4)

let userName = 'gamer2022';
let userName2 = 'shadow1991'
let userAge = 15;
let userAge2 = 30;

let registration = userName.length > 8 && userAge >= requiredAge;
//.length is a property of strings which determine the number of characters in the string
console.log(registration) //false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

console.log("Logical OR Operator")
//OR Operator (|| - Double pipe)
//returns true if atleast ONE of the operands are true

let userLeve1 = 100
let userLeve2 = 65

let guildRequiment1 = isRegistered || userLeve2 >= requiredLevel || userAge2>= requiredAge;
console.log(guildRequiment1) //true

let guildRequiment2 = isAdmin || userLeve2 >= requiredLevel;
console.log(guildRequiment2) //false


console.log("Not Operator")
//Not Operator (!)
//it turns a boolean into the opposite value

let guildAdmin = !isAdmin || userLeve2 >= requiredLevel;
console.log(guildAdmin); //true

console.log(!isRegistered); //false

console.log(!isLegalAge);

// let opposite = !isAdmin;
// let opposite2 = !isLegalAge;

// console.log = (opposite)//true - isAdmin original value - false
// console.log(opposite2); //opposite - isLegalAge orinal value = true


//if, else if, and else statement

//IF statement
//is statement will run a code block if the condition specified is true or results to true

let numG= -1;
if(numG < 0){
	console.log("Hello")
}

let userName3 = "crusader_1993"
let userLeve13 = 25;
let userAge3 = 20;

if(userName3.length >10){
	console.log("Welcome to Game Online!")
}
if(userLeve13 >= requiredLevel){
	console.log("You are qualified to join the guild!")
}
if (userName3.length >= 10 && isRegistered && isAdmin){
	console.log("THank you for joining the admin!")
} else {
	console.log("You are not ready to be an admin")
}

//ELSE statement
//the "else" statements executes a block of codes if all other conditions are false

if(userName3.length >= 10 && userLeve13 >= requiredLevel && userAge3 >= requiredAge){
	console.log("Thank you for joining the Noobies Guild")
} else{
	console.log("you are too strong to be a noob")
}

//else-if statements
//else if executes a statements if the previous or the original condition is false or resulted to false but another specified condition resulted to true.
if(userName3.length >= 10 && userLeve13 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the noobies")
} else if(userLeve1 < requiredAge){
	console.log("You are too young to join the guild.")
} else{
	console.log("better luck next time")
}

//if, else if, and else statements with functions
function addNumber(num1, num2){
	//check if the numbers beign passed are number types
	//typeof keyword returns a string which tells the type of data that follows it
	if(typeof num1 === "number" && typeof num2 === "number") {
		console.log("Run only if both arguments passed are number types")
		console.log(num1 + num2)
	} else{
		console.log("One or both of the arguments are not numbers")
	}
}

addNumber(5, '2')
let numSample = 5;
console.log(typeof numSample)

//create log in function
function login(username, password){
	//check if the arguments passed are strings
	if(typeof username === 'string' && typeof password === "string"){
		console.log("Both arguments are strings.")
		/*
			nested if-else
				will run if the parent if statement is able to aggree to accomplish its condition
		*/
		 if(username.length >= 8 && password.length >= 8){
			console.log("Thank you for logging in")
		} 

		else if(username.length <= 8){
			console.log("Username is too short")
		} else if(password.length <= 8){
			console.log("password is too short")
		}
	}
		else{
			console.log("Credentials too short")
		} 
		}
		login("sdssdeds", "23211111")



//function with return keyword
let message = "No Message."
console.log(message)

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return 'not a typhoon yet.';
	}
	else if(windSpeed <=61){
		return 'Tropical depression detected'
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected"
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical storm detected'
	}
	else {
		return 'Typhoon detected.'
	}
}

message = determineTyphoonIntensity(68)
console.log(message)

//console.warn() is a good way to print warnings in our console that could help us developers act on certain within our code
if (message == 'Tropical storm detected'){
	console.warn(message);
}


//Truthy and Falsy

if (true){
	console.log('Truthy')
}
if(1){
	console.log('true')
}
if([]){
	console.log('Truthy')
}



//Falsy
//-0, "", null, NaN
if(false){
	console.log('Falsy')
}
if (0) {
	console.log('False')
}
if(undefined){
	console.log('False')
}


//Ternary Operator
/*
Syntax: 
	(expression/condition) ? iftrue : iffalse
three operands of ternary operator:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is false
*/

let ternaryResult = (1 < 18) ? true : false
console.log(`Result of ternary operator ${ternaryResult}`)
//LIKE
if(1 < 18){
	console.log(true)
} else{
	console.log(false)
}

let price = 50000;
price > 1000 ? console.log("price is over 1000") : console.log("price is less than 1000")

let villain = "Harvey Dent"
villain === "Two Face"
? console.log("You lived long enough to be villain")
: console.log("Not Quite villainous yet.")

//Ternary operators have an implicit "return" statement that without return keyword resulting expression can be stored in a variable


//else if ternary operator
let a = 7
 a === 5
? console.log("A")
: (a === 10) ? console.log("A is 10") : console.log("A is not 5 or 10");


let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit'
}

function isUnderAge(){
	name = "Jane";
	return 'You are under the age limit';
}

let age = parseInt(("What is your age?"))
console.log(age)

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log(`Result of Ternary operator in functions: ${legalAge}, ${name}`)





// let dayToday = prompt("enter a day")
// function colorEnter(){	

// 	if(typeof dayToday === 'string'){

// 	if(dayToday.toLowerCase() === "monday"){
// 		alert(`Today is Monday, Wear a ${dayToday}`)

// 	 }
// 	else if(dayToday.toLowerCase() === 'tuesday'){
// 		alert(`Today is Tuesday, Wear a ${dayToday}`)
// 	}
// 	else if(dayToday.toLowerCase() === 'wednesday'){
// 		alert(`Today is Wednesday, Wear a ${dayToday}`)
// 	}
// 	else if(dayToday.toLowerCase() === 'thursday'){
// 		alert(`Today is Thursday, Wear a ${dayToday}`)
// 	}
// 	else if(dayToday.toLowerCase() === 'friday'){
// 		alert(`Today is Friday, Wear a ${dayToday}`)
// 	}
// 	else if(dayToday.toLowerCase() === 'saturday'){
// 		alert(`Today is Saturday, Wear a ${dayToday}`)
// 	}
// 	else if(dayToday.toLowerCase() === 'sunday'){
// 		alert(`Today is Sunday, Wear a ${dayToday}`)
// 	} else{
// 		alert("Invalid Input. Enter a valid day of the Week")
// 	}
// }
	
// 	else{
// 		alert("Invalid Input. Enter a valid day of the Week")
// 	}	
// }

// colorEnter();



//Switch Statement
/*
Syntax: 	
	switch (condition){
	case value:
		statement;
		break;
	default:
		statement;
		break;
	}

*/

// // let hero = prompt("Type a Hero:").toLowerCase()

// switch (hero){
// 	case "jose rizal":
// 		console.log("National Hero of the Philippines")
// 		break
// 	case "George Washington":
// 		console.log("Hero of the American Revolution")
// 		break
// 	case "Hercules":
// 		console.log("Legendary Hero of the greek")
// 		break

// 	default:
// 		console.log("Please Type again")
// }

function roleChecker (role){
	switch(role){
		case "admin":
			console.log("Welcome admin, to the dashboard.")
		break
		case "user":
		console.log("You are not authorized to view this page")
		break
		case "guest":
		console.log("Go to the registration page to enter.")

		default:
		console.log("Invalid Role")
		//by default, your switch ends with default case, so therefore, even if there is no break keyword in your default case, it will not run anything else.
	}
}
roleChecker("admin")


//Try-Catch-Finally Statement
//this is used for error handling

function showIntensityAlert(windSpeed){
	//Attemp to execute a code
	try{
		alerat(determineTyphoonIntensity(68))
	} catch(error){
		//error/err are commonly used variable by developers for storing errors
		console.log(typeof error)
		
		//Catch errors within 'try' statement
		//the "error.message" is used to access the information relating to an error object
		console.warn(error.message)
	}
	finally {
		//Continue execution of code REGARDLESS of success or failure of code excution in the 'try' block to handle/resolve errors
		alert('Intensity updates will show new alert')
	}
}

showIntensityAlert(68)

//throw - user-defined exception
//Execution of the current function will stop

const number = 40;
try{
	if(number > 50){
		console.log('Success')}
			else{
				//user-defined throw statements
				throw Error('The number is low')
			}
			//if throw executes the below code does not execute
			console.log('Hello')
		
}catch(error){
	console.log('An error caught')
	console.log(error.message)
}
finally{
	console.log("Please add a higher number")
}


//another example
function getArea(width, height){
	if(isNaN(width) || isNaN(height)){
		throw 'Parameter is not a number!'
	}
}

try{
	getArea(3, 'A')
}catch(e){
console.log(e)
}finally{
	alert("Number only")
}